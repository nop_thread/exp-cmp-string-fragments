# Experiments on fragmented strings comparison

[![Build Status](https://gitlab.com/lo48576/exp-cmp-strings-fragments/badges/develop/pipeline.svg)](https://gitlab.com/lo48576/exp-cmp-strings-fragments/pipelines/)
![Minimum supported rustc version: 1.51](https://img.shields.io/badge/rustc-1.51+-lightgray.svg)

Experimental implementation of fragmented strings comparison.

Functions in this crate compares the iterator of strings as if they are joined.
For example, `eq_multi_multi(&["foo", "bar", "baz"], &["foob", "", "a", "rbaz"])` returns `true`.

## Why experimental?

There are more things which should be done, but I don't feel like doing them.

* Benchmarking
    + Is it really fast?
    + `fragments1.flat_map(|s| s.bytes()).eq(fragments2.flat_map(|s| s.bytes()))`
      is very simple and well understandable.
      If the performance difference is ignorable, simpler one should be used.
* Tests and fuzzing
    + Is it really correct?
    + I roughly wrote tests, but they would be not enough.
    + Fuzzing would be quite useful to test this kind of functions.

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)
* CC-1.0 ([LICENSE-CC0.txt](LICENSE-CC0.txt) or
  <https://creativecommons.org/publicdomain/zero/1.0/>)

at your option.

<!-- Specifying other licenses with CC0-1.0 seems useless, but this is intended. -->

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
