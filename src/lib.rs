//! Experiments on fragmented strings comparison.
#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]
// `clippy::missing_docs_in_private_items` implies `missing_docs`.
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::unwrap_used)]
#![no_std]

use core::cmp::{self, Ordering};

/// Compares a concatenated string and a single string.
///
/// This returns the same value as `fragments.into_iter().collect::<String>() == s` and
/// `fragments.flat_map(|s| s.bytes()).eq(s.bytes())`.
/// but computes this without memory allocations.
#[must_use]
pub fn eq_multi_single<'a, I>(fragments: I, s: &str) -> bool
where
    I: IntoIterator<Item = &'a str>,
{
    eq_multi_single_impl(fragments.into_iter().filter(|s| !s.is_empty()), s)
}

/// Compares a concatenated string and a single string.
///
/// This returns the same value as `fragments.collect::<String>() == s`,
/// but computes this without memory allocations.
///
/// Note that the given iterator should not return `Some("")`.
#[inline] // Used at only one place.
#[must_use]
fn eq_multi_single_impl<'a, I>(fragments: I, s: &str) -> bool
where
    I: Iterator<Item = &'a str>,
{
    let mut s_rest = s;
    for frag in fragments {
        match s_rest.strip_prefix(frag) {
            Some(s_suffix) => s_rest = s_suffix,
            None => return false,
        }
    }
    s_rest.is_empty()
}

/// Compares multiple concatenated strings.
///
/// This returns the same value as `lhs_fragments.into_iter().collect::<String>() ==
/// rhs_fragments.into_iter().collect::<String>()`,
/// but computes this without memory allocations.
#[must_use]
pub fn eq_multi_multi<'a, I, J>(fragments1: I, fragments2: J) -> bool
where
    I: IntoIterator<Item = &'a str>,
    J: IntoIterator<Item = &'a str>,
{
    eq_multi_multi_impl(
        fragments1.into_iter().filter(|s| !s.is_empty()),
        fragments2.into_iter().filter(|s| !s.is_empty()),
    )
}

/// Compares multiple concatenated strings.
///
/// This returns the same value as `fragments1.into_iter().collect::<String>() ==
/// fragments2.into_iter().collect::<String>()`,
/// but computes this without memory allocations.
///
/// Note that the given iterators should not return `Some("")`.
#[inline] // Used at only one place.
#[must_use]
fn eq_multi_multi_impl<'a, I, J>(mut fragments1: I, mut fragments2: J) -> bool
where
    I: Iterator<Item = &'a str>,
    J: Iterator<Item = &'a str>,
{
    let (mut buf1, mut buf2) = match (fragments1.next(), fragments2.next()) {
        (Some(l), Some(r)) => (l, r),
        // Both strings are empty.
        (None, None) => return true,
        // Only one of the strings is empty.
        _ => return false,
    };

    loop {
        debug_assert!(!buf1.is_empty());
        debug_assert!(!buf2.is_empty());

        let buf1_is_short = buf1.len() < buf2.len();
        let (short_buf, long_buf) = if buf1_is_short {
            (&mut buf1, &mut buf2)
        } else {
            (&mut buf2, &mut buf1)
        };
        // Now `long_buf.len() >= short_buf.len()` is satisfied.
        *long_buf = match long_buf.strip_prefix(*short_buf) {
            Some(suf) => suf,
            None => {
                // Difference is found.
                return false;
            }
        };
        *short_buf = "";

        // Fill the buffers.
        if buf1.is_empty() {
            buf1 = fragments1.next().unwrap_or("");
        };
        if buf2.is_empty() {
            buf2 = fragments2.next().unwrap_or("");
        };
        if buf1_is_short && buf1.is_empty() {
            return buf2.is_empty();
        } else if !buf1_is_short && buf2.is_empty() {
            return buf1.is_empty();
        }
    }
}

/// Compares multiple concatenated strings.
///
/// This returns the same value as
/// `fragments1.into_iter().collect::<String>().partial_cmp(fragments2.into_iter().collect::<String>().unwrap()`
/// and `fragments1.flat_map(|s| s.bytes()).partial_cmp(fragments2.flat_map(|s| s.bytes())).unwrap()`,
/// but computes this without memory allocations.
#[must_use]
pub fn cmp_multi_multi<'a, I, J>(fragments1: I, fragments2: J) -> Ordering
where
    I: IntoIterator<Item = &'a str>,
    J: IntoIterator<Item = &'a str>,
{
    cmp_multi_multi_impl(
        fragments1.into_iter().filter(|s| !s.is_empty()),
        fragments2.into_iter().filter(|s| !s.is_empty()),
    )
}

/// Compares multiple concatenated strings.
///
/// This returns the same value as
/// `fragments1.into_iter().collect::<String>().partial_cmp(fragments2.into_iter().collect::<String>().unwrap()`
/// and `fragments1.flat_map(|s| s.bytes()).partial_cmp(fragments2.flat_map(|s| s.bytes())).unwrap()`,
/// but computes this without memory allocations.
///
/// Note that the given iterators should not return `Some("")`.
#[inline] // Used at only one place.
#[must_use]
fn cmp_multi_multi_impl<'a, I, J>(mut lhs_fragments: I, mut rhs_fragments: J) -> Ordering
where
    I: Iterator<Item = &'a str>,
    J: Iterator<Item = &'a str>,
{
    let (mut lhs_buf, mut rhs_buf): (&str, &str) =
        match (lhs_fragments.next(), rhs_fragments.next()) {
            (Some(lhs), Some(rhs)) => (lhs, rhs),
            (Some(_), None) => return Ordering::Greater,
            (None, Some(_)) => return Ordering::Less,
            (None, None) => return Ordering::Equal,
        };

    loop {
        // Retrieve the next fragment when buffers are empty.
        if lhs_buf.is_empty() {
            lhs_buf = lhs_fragments.next().unwrap_or("");
        }
        if rhs_buf.is_empty() {
            rhs_buf = rhs_fragments.next().unwrap_or("");
        }

        // If either of the string is empty, the string has been compared to the end.
        {
            let lhs_empty = lhs_buf.is_empty();
            let rhs_empty = rhs_buf.is_empty();
            if lhs_empty || rhs_empty {
                return rhs_empty.cmp(&lhs_empty);
            }
        }

        // Both lhs and rhs have fragments to be compared.
        let common_len = cmp::min(lhs_buf.len(), rhs_buf.len());
        let (lhs_prefix, lhs_suffix) = lhs_buf.split_at(common_len);
        let (rhs_prefix, rhs_suffix) = rhs_buf.split_at(common_len);

        match lhs_prefix.cmp(rhs_prefix) {
            Ordering::Equal => {
                // Compare more fragments.
                lhs_buf = lhs_suffix;
                rhs_buf = rhs_suffix;
                continue;
            }
            ordering => {
                // Difference is found.
                return ordering;
            }
        }
        // Unreachable here.
    }
    // Unreachable here.
}

#[cfg(test)]
mod tests {
    use super::*;

    extern crate alloc;

    use core::mem;

    use alloc::string::String;
    use alloc::vec;

    /// Calls the given function with the splitted strings.
    ///
    /// This does not put empty strings between other string fragments.
    fn test_with_all_possible_splits<F: Fn(&[&str])>(s: &str, f: F) {
        let s_len = s.len();
        let num_chars = s.chars().count();
        assert!(num_chars <= mem::size_of::<usize>());

        let mut fragments = vec![""; num_chars];
        let mut char_counts = vec![0; num_chars];
        for bits in 0..=!(!0 << (num_chars - 1)) {
            let mut frag_i = 0;
            char_counts[frag_i] = 1;
            for char_i in 1..num_chars {
                if bits & (1 << (char_i - 1)) != 0 {
                    frag_i += 1;
                    char_counts[frag_i] = 0;
                }
                char_counts[frag_i] += 1;
            }

            let mut start = 0;
            let mut chars = s.char_indices().skip(1);
            for (count, frag) in char_counts.iter().take(frag_i + 1).zip(&mut fragments) {
                let end = chars
                    .by_ref()
                    .skip(*count - 1)
                    .next()
                    .map(|(idx, _ch)| idx)
                    .unwrap_or(s_len);
                *frag = &s[start..end];
                start = end;
            }
            assert_eq!(start, s_len);

            f(&fragments[..=frag_i]);
        }
    }

    #[test]
    fn test_eq_multi_single() {
        fn test(frags: &[&str], s: &str) {
            assert_eq!(
                eq_multi_single(frags.iter().copied(), &s),
                frags.iter().flat_map(|s| s.bytes()).eq(s.bytes()),
                "fragments={:?}, s={:?}",
                frags,
                s
            );
        }

        fn test_self(frags: &[&str]) {
            let s = frags.iter().copied().collect::<String>();
            test(frags, &s);
        }

        test_self(&[]);
        test_self(&[""]);
        test_self(&["", ""]);

        let single_strings = &[
            "", "aoobar", "fooba", "foobaa", "foobaaa", "foobar", "foobara", "foobaraa", "foobarz",
            "foobaz", "foobarz", "foobarzz", "zoobar",
        ];
        test_with_all_possible_splits("foobar", |frags| {
            for s in single_strings {
                test(frags, s);
            }
        });
    }

    #[test]
    fn test_eq_multi_multi() {
        fn test(frags1: &[&str], frags2: &[&str]) {
            assert_eq!(
                eq_multi_multi(frags1.iter().copied(), frags2.iter().copied()),
                frags1
                    .iter()
                    .flat_map(|s| s.bytes())
                    .eq(frags2.iter().flat_map(|s| s.bytes())),
                "fragments1={:?}, fragments2={:?}",
                frags1,
                frags2
            );
        }

        fn test_self(frags: &[&str]) {
            let s = frags.iter().copied().collect::<String>();
            test(frags, &[s.as_str()]);
        }

        test_self(&[]);
        test_self(&[""]);
        test_self(&["", ""]);

        let single_strings = &[
            "", "aoobar", "fooba", "foobaa", "foobaaa", "foobar", "foobara", "foobaraa", "foobarz",
            "foobaz", "foobarz", "foobarzz", "zoobar",
        ];
        test_with_all_possible_splits("foobar", |frags| {
            for s in single_strings {
                test(frags, &[s]);

                let s_len = s.len();
                // Assume that `s` is an ASCII string.
                let (prefix, suffix) = s.split_at(s_len / 2);
                test(frags, &[prefix, suffix]);
                let (prefix, suffix) = s.split_at(s_len / 3);
                test(frags, &[prefix, suffix]);
                let (prefix, suffix) = s.split_at(s_len / 4);
                test(frags, &[prefix, suffix]);
            }
        });
    }

    #[test]
    fn test_cmp_multi_multi() {
        fn test(frags1: &[&str], frags2: &[&str]) {
            assert_eq!(
                cmp_multi_multi(frags1.iter().copied(), frags2.iter().copied()),
                frags1
                    .iter()
                    .flat_map(|s| s.bytes())
                    .cmp(frags2.iter().flat_map(|s| s.bytes())),
                "fragments1={:?}, fragments2={:?}",
                frags1,
                frags2
            );
        }

        fn test_self(frags: &[&str]) {
            let s = frags.iter().copied().collect::<String>();
            test(frags, &[s.as_str()]);
        }

        test_self(&[]);
        test_self(&[""]);
        test_self(&["", ""]);

        let single_strings = &[
            "", "aoobar", "fooba", "foobaa", "foobaaa", "foobar", "foobara", "foobaraa", "foobarz",
            "foobaz", "foobarz", "foobarzz", "zoobar",
        ];
        test_with_all_possible_splits("foobar", |frags| {
            for s in single_strings {
                test(frags, &[s]);

                let s_len = s.len();
                // Assume that `s` is an ASCII string.
                let (prefix, suffix) = s.split_at(s_len / 2);
                test(frags, &[prefix, suffix]);
                let (prefix, suffix) = s.split_at(s_len / 3);
                test(frags, &[prefix, suffix]);
                let (prefix, suffix) = s.split_at(s_len / 4);
                test(frags, &[prefix, suffix]);
            }
        });
    }
}
